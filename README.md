# dockerized-jupyter

scripts d'installation de docker, docker-compose et déploiement conteneur jupyter datascience par ansible

Script de lancement du notebook notebook-launch.sh:

. notebook-launch.sh param1 param2
param1: volume shared with the docker container (required)
param2: local machine or VM's port to share with the container's port 8888

https://jupyter-docker-stacks.readthedocs.io/en/latest/using/common.html#Docker-Options

Jupyter notebook is a very useful tool for scientists during the data exploration phase allowing to get nice preliminary results with a few lines of Python code for instance.
As a former scientist I know that access to programming tools is key to getting the most out of collected experimental data
unfortunately the installation of those tools may be challenging.
This repo contains ansible files to intall docker-ce and build a docker container running a jupyter notebook.
The jupyter notebook can be accessed with an HTTP client like mozilla on you local machine.
Requirements:
Centos7
Script de lancement du notebook notebook-launch.sh:
. notebook-launch.sh param1 param2
param1: volume shared with the docker container (required)
param2: local machine or VM's port to share with the container's port 8888 
