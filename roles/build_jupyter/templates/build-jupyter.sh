#! /bin/bash

#script de démarrage de Jupyter notebook datascience conteneurisé

#start the docker daemon
sudo systemctl start docker
#build the jupyter container


echo "---!!!!!! the container will start building shortly, when done, open it with you favorite HTTP browser with the indicated url (see below)\
http://127.0.0.1:8888token. If you run the container in a VM dont forget to use the VM's IP adress!" 

sleep 10

echo "---------starting to build"


if [ $1 ]
then
        docker run -p $1:8888 -v /home/datascience:/home/jovyan/work jupyter/datascience-notebook
else
	docker run -p 8888:8888 -v /home/datascience:/home/jovyan/work jupyter/datascience-notebook
fi

